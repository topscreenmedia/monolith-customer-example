(function(angular) {
  function PerkProvider($q, $interval) {
    var service = this,
        cache = {},
        userCache = {},
        subs = {},
        promises = {};

    function getCachedUserPerk(perk) {
      var merchId = perk.get('merchant').id;

      for (var i in userCache[merchId]) {
        if (!userCache[merchId].hasOwnProperty(i)) continue;
        if (perk.id !== userCache[merchId][i].get('perk').id) continue;

        return userCache[merchId][i]
      }

      return null;
    }

    function setCachedUserPerk(openPerk) {
      var merchId = openPerk.get('merchant').id;

      for (var i in userCache[merchId]) {
        if (!userCache[merchId].hasOwnProperty(i)) continue;
        if (openPerk.id !== userCache[merchId][i].id) continue;

        userCache[merchId][i] = openPerk;

        return userCache[merchId].length;
      }

      return userCache[merchId].push(openPerk);
    }

    function generateOpenPerkQuery(perk, user) {
      var cached = getCachedUserPerk(perk);
      var qu = new Parse.Query('OpenPerk');
      qu.equalTo('perk', perk);
      qu.equalTo('customer', user);

      if (perk.get('isIntroPerk')) {
        qu.notEqualTo('redemptionCount', 0);
      } else if (cached) {
        // "notEqualTo" because stampCount could go up but
        // could also go down if it's a full redemption
        qu.notEqualTo('stampCount', cached.get('stampCount'));
      } else {
        qu.notEqualTo('stampCount', 0);
      }

      return qu;
    }

    service.getForShop = function(shop) {
      if (cache[shop.id])
        return $q.when(cache[shop.id]);

      var qu = new Parse.Query('Perk'),
          deferred = $q.defer();

      qu.equalTo('shops', shop);
      qu.notEqualTo('inactive', true);

      qu.find().then(function (perks) {
        cache[shop.id] = perks;
        deferred.resolve(cache[shop.id]);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    service.getForUserShop = function (shop, user) {
      var merchant = shop.get('merchant');

      if (userCache[merchant.id])
        return $q.when(userCache[merchant.id]);

      var qu = new Parse.Query('OpenPerk'),
          deferred = $q.defer();

      qu.equalTo('customer', user);
      qu.equalTo('merchant', merchant);

      qu.find().then(function (openPerks) {
        userCache[merchant.id] = openPerks;
        deferred.resolve(userCache[merchant.id]);
      }, function (error) {
        deferred.reject(error);
      });

      return deferred.promise;
    };

    // long-poll the server
    // when we have our own implementation of Parse Server
    // we can use a websocket through Parse LiveQueries
    service.waitForUserPerkUpdate = function (perk, user) {
      if (service.hasSub(perk))
        $interval.cancel(subs[perk.id]);

      var deferred = $q.defer();
      var qu = generateOpenPerkQuery(perk, user);

      // will try every 5 secs for 8 mins or until cancelled
      subs[perk.id] = $interval(function() {
        try {
          // $$status is private API so can't necessarily be trusted.
          // moreover this really shouldn't be necessary
          if (deferred.promise.$$state.status) return;
        } catch (ex) {
          console.debug(ex);
        }
        qu.find().then(function (openPerks) {
          if (!openPerks.length) return;
          setCachedUserPerk(openPerks[0]);
          service.cancelSub(perk);
          deferred.resolve(openPerks[0]);
        }, function (error) {
          service.cancelSub(perk);
          deferred.reject(error);
        });
      }, 5000, 8640).then(function () {
        // 8640 cycles of 5s = 12 hours which is the length of time
        // which check-ins stay on the merchant app.
        deferred.reject('No response from merchant');
      });

      promises[perk.id] = deferred.promise;
      return promises[perk.id];
    };

    service.hasPromise = function(perk) {
      return promises.hasOwnProperty(perk.id);
    }

    service.getPromise = function(perk) {
      return promises[perk.id];
    }

    service.hasSub = function(perk) {
      return subs.hasOwnProperty(perk.id);
    };

    service.cancelSub = function (perk) {
      if (!perk.hasOwnProperty('id')) return false;
      if (!service.hasSub(perk)) return false;

      $interval.cancel(subs[perk.id]);
      delete subs[perk.id];
      delete promises[perk.id];
    };

    service.teardown = function () {
      for (var i in cache) {
        if (!cache.hasOwnProperty(i)) continue;
        service.cancelSub(cache[i]);
      }

      cache = {};
      userCache = {};
    };
  }

  angular.module('application')
    .service('PerkProvider', PerkProvider);
})(angular);