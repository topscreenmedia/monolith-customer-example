(function (angular) {
  function ShopPerksController($q, $scope, $state, $ionicLoading, $ionicPopup, PositionProvider, PerkProvider, shop) {
    var vm = this;

    $scope.$on('$ionicView.loaded', onFirstLoad);

    function onFirstLoad() {
      $ionicLoading.show({template: '<ion-spinner />'});

      vm.perks = [];
      vm.openPerks = {};
      vm.showInfo = {};

      var qShopPerks = PerkProvider.getForShop(shop),
          qUserShopPerks = PerkProvider.getForUserShop(shop, Parse.User.current());

      qShopPerks.then(function(perks) {
        vm.perks = perks;
        perks.forEach(function (perk) {
          if (!PerkProvider.hasPromise(perk)) return;

          PerkProvider.getPromise(perk)
            .then(handleValidationSuccess, window.logError);
        });
        vm.showInfo = perks.reduce(function (o, perk) {
          o[perk.id] = false;
          return o;
        }, {});
      });

      qUserShopPerks.then(function (openPerks) {
        vm.openPerks = openPerks.reduce(function (o, openPerk) {
          o[openPerk.get('perk').id] = openPerk;
          return o;
        }, {});
      });

      $q.all([qShopPerks, qUserShopPerks])
        .catch(window.logError)
        .finally($ionicLoading.hide);
    }

    function handleValidationSuccess(openPerk) {
      vm.openPerks[openPerk.get('perk').id] = openPerk;
    }

    vm.isPending = PerkProvider.hasSub;

    vm.getOpenPerkForPerk = function(perk) {
      return vm.openPerks[perk.id];
    };

    vm.shouldDisplay = function (perk) {
      var openPerk = vm.getOpenPerkForPerk(perk);
      if (!openPerk) return true;
      return !(perk.get('isIntroPerk') && openPerk.get('redemptionCount') > 0)
    };

    vm.isIntroPerk = function(perk) {
      return perk.get('isIntroPerk') ? 0 : 1;
    };

    vm.getNumberArray = function(perk) {
      var numbers = [];
      for (var i = 0; i < perk.get('target'); i++) {
        numbers.push(i + 1);
      }
      return numbers;
    };

    vm.checkIn = function(perk) {
      $ionicLoading.show({template: '<ion-spinner/>'});
      PositionProvider.getCurrent().then(function(position) {
        var checkin = new Parse.Object('CheckIn');
        checkin.set('customer', Parse.User.current());
        checkin.set('shop', shop);
        checkin.set('perk', perk);
        checkin.set('merchant', shop.get('merchant'));
        checkin.set('location', new Parse.GeoPoint(position.coords));
        checkin.set('validated', false);

        return checkin.save();
      }).then(function() {
        var openPerk = vm.getOpenPerkForPerk(perk);
        $ionicLoading.hide();

        if (openPerk && openPerk.get('stampCount') == perk.get('target') - 1) {
          $state.go('app.checkincompleted');
        } else $state.go('app.checkinconfirmed');

        return PerkProvider.waitForUserPerkUpdate(perk, Parse.User.current());
      }).then(handleValidationSuccess, window.logError);
    };
  }

  angular.module('application.controllers')
    .controller('ShopPerksController', ShopPerksController);
})(angular);

