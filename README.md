# WLL Monolith Customer Example

This repository contains some example frontend code for rendering and interacting with the White Label Loyalty Monolith system.

This is not an entire working application but provides some example code and logic.

The code is based on AngularJS 1.5.3.